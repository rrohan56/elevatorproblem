Instructions to run the application:

1) Clone the ElevatorProblem project from bit bucket
2) Run the ElevatorExecutor.java class with the following program arguments as shown below. 	
	/path/to/the/input/file modeName
 For example: src/test/resources/inputs.txt modea or 
	      src/test/resources/inputs.txt modeb

3) The input file is already added to the folder src/test/resources. You can use the same file or add the input file at some other location.The path to the input file is relative to the directory where the project is extracted or cloned.
