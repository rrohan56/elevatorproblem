package com.elevator.exceptions;

/**
 * @author Rohan Raghuwanshi (rraghuw)
 *
 * 
 */
public class InvalidNumber extends Exception {

  private static final long serialVersionUID = 8389827059020966663L;

  public InvalidNumber(String msg) {
    super(msg);
  }
}
