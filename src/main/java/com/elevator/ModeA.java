package com.elevator;

import java.util.Queue;

import com.elevator.exceptions.InvalidNumber;
import com.elevator.impl.ElevatorSystemFactoryImpl;

/**
 * @author Rohan Raghuwanshi (rraghuw)
 *
 * 
 */
public class ModeA {

  Queue<Integer> currentLocation;
  Queue<Integer> startFloor;
  Queue<Integer> endFloor;
  int floorCount = 0;
  int initialFloorLocation = 0;
  public static final int NUMBER_OF_FLOORS = 12;
  

  public int getInitialFloorLocation() {
    return initialFloorLocation;
  }

  public void setInitialFloorLocation(int initialFloorLocation) {
    this.initialFloorLocation = initialFloorLocation;
  }

  public int getFloorCount() {
    return floorCount;
  }

  public void setFloorCount(int floorCount) {
    this.floorCount = floorCount;
  }

  public Queue<Integer> getCurrentLocation() {
    return currentLocation;
  }

  public void setCurrentLocation(Queue<Integer> currentLocation) {
    this.currentLocation = currentLocation;
  }

  public Queue<Integer> getStartFloor() {
    return startFloor;
  }

  public void setStartFloor(Queue<Integer> startFloor) {
    this.startFloor = startFloor;
  }

  public Queue<Integer> getEndFloor() {
    return endFloor;
  }

  public void setEndFloor(Queue<Integer> endFloor) {
    this.endFloor = endFloor;
  }

  public void getElevatorOutput() throws InvalidNumber {
    
    int floorCount = 0;
    ElevatorSystemFactoryImpl elevatorSystemFactory = new ElevatorSystemFactoryImpl(NUMBER_OF_FLOORS, currentLocation.peek());
     while (!startFloor.isEmpty()) {
      elevatorSystemFactory.pickUp(startFloor.poll());
      elevatorSystemFactory.destination(endFloor.poll());
      floorCount =+ elevatorSystemFactory.getFloorCountModeA();

    }
     setFloorCount(floorCount);
     setInitialFloorLocation(currentLocation.poll());
   
  }

}
