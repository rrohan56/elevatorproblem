package com.elevator.impl;

import com.elevator.ElevatorDirection;
import com.elevator.core.ElevatorFactory;

/**
 * @author Rohan Raghuwanshi (rraghuw)
 *
 * 
 */

public class ElevatorFactoryImpl implements ElevatorFactory {
  private Integer currentFloor;
  ElevatorDirection dir;

  public ElevatorFactoryImpl(Integer currentFloor) {
    this.currentFloor = currentFloor;
  }

  public void setCurrentFloor(Integer currentFloor) {
    this.currentFloor = currentFloor;

  }

  public ElevatorDirection getDir() {
    return dir;
  }

  public void setDir(ElevatorDirection dir) {
    this.dir = dir;
  }

  public int getCurrentFloor() {
    return this.currentFloor;
  }

  @Override
  public void goUp() {
    currentFloor++;
  }

  @Override
  public void goDown() {
    currentFloor--;
  }

  public void moveUp() {
    dir = ElevatorDirection.UP;
  }

  public void moveDown() {
    dir = ElevatorDirection.DOWN;
  }

}
