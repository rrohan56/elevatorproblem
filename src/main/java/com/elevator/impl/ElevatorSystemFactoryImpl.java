package com.elevator.impl;

import java.util.LinkedList;
import java.util.Queue;
import java.util.TreeSet;

import com.elevator.ElevatorDirection;
import com.elevator.core.ElevatorSystemFactory;
import com.elevator.exceptions.InvalidNumber;

/**
 * @author Rohan Raghuwanshi (rraghuw)
 *
 * 
 */
public class ElevatorSystemFactoryImpl implements ElevatorSystemFactory {

  private Integer numberOfFloors = 0;
  private static ElevatorFactoryImpl elevator;
  private Queue<Integer> pickupLocations;
  private Queue<Integer> dropOffLocations;
  private Integer currentFloor;
  private int floorCount = 0;
  private static int distance = 0;
  private static TreeSet<Integer> up = new TreeSet();;
  private static TreeSet<Integer> down = new TreeSet();

  public ElevatorSystemFactoryImpl(int numberOfFloors, int currentFloor) throws InvalidNumber {
    this.numberOfFloors = numberOfFloors;
    this.currentFloor = currentFloor;
    elevator = new ElevatorFactoryImpl(currentFloor);
    pickupLocations = new LinkedList<Integer>();
    dropOffLocations = new LinkedList<Integer>();
  }

  public static int getDistance() {
    return distance;
  }

  public static void setDistance(int newDistance) {
    distance = newDistance;
  }

  public Queue<Integer> getPickupLocations() {
    return pickupLocations;
  }

  public void setPickupLocations(Queue<Integer> pickupLocations) {
    this.pickupLocations = pickupLocations;
  }

  public Queue<Integer> getDropOffLocations() {
    return dropOffLocations;
  }

  public void setDropOffLocations(Queue<Integer> dropOffLocations) {
    this.dropOffLocations = dropOffLocations;
  }

  @Override
  public void pickUp(Integer pickUpFloor) {
    pickupLocations.add(pickUpFloor);
  }

  @Override
  public void destination(Integer destinationFloor) {
    dropOffLocations.add(destinationFloor);
  }

  public ElevatorFactoryImpl getElevator() {
    return elevator;
  }

  public void setElevator(ElevatorFactoryImpl elevator) {
    this.elevator = elevator;
  }

  @Override
  public int getFloorCountModeA() {

    if (!pickupLocations.isEmpty()) {
      while (elevator.getCurrentFloor() > pickupLocations.peek()) {
        elevator.goDown();
        floorCount++;
      }

      while (elevator.getCurrentFloor() < pickupLocations.peek()) {
        elevator.goUp();
        floorCount++;
      }
    }
    pickupLocations.remove();

    if (!dropOffLocations.isEmpty()) {
      while (elevator.getCurrentFloor() > dropOffLocations.peek()) {
        elevator.goDown();
        floorCount++;
      }
      while (elevator.getCurrentFloor() < dropOffLocations.peek()) {
        elevator.goUp();
        floorCount++;
      }
    }
    dropOffLocations.remove();
    return floorCount;
  }

  @Override
  public int getFloorCountModeB() {

    while (!pickupLocations.isEmpty()) {
      if (elevator.getCurrentFloor() > pickupLocations.peek()) {
        elevator.moveDown();
        down.add(pickupLocations.peek());
        elevator.setCurrentFloor(nextFloor());

      } else {
        elevator.moveUp();
        ;
        up.add(pickupLocations.peek());
        elevator.setCurrentFloor(nextFloor());
      }
      pickupLocations.remove();
    }

    while (!dropOffLocations.isEmpty()) {
      if (elevator.getCurrentFloor() > dropOffLocations.peek()) {
        elevator.moveDown();
        down.add(dropOffLocations.peek());
        elevator.setCurrentFloor(nextFloor());

      } else {
        elevator.moveUp();
        up.add(dropOffLocations.peek());
        elevator.setCurrentFloor(nextFloor());
      }
      dropOffLocations.remove();
    }
    return getDistance();

  }

  public static int nextFloor() {

    if (elevator.getDir() == ElevatorDirection.DOWN) {
      System.out.print(down.first() + " ");
      setDistance(elevator.getCurrentFloor() > down.first() ? elevator.getCurrentFloor() - down.first() : down.first()
          - elevator.getCurrentFloor());
      return (int) down.pollLast();
    } else {
      System.out.print(up.first() + " ");
      setDistance(elevator.getCurrentFloor() > up.first() ? elevator.getCurrentFloor() - up.first() : up.first()
          - elevator.getCurrentFloor());
      return (int) up.pollFirst();
    }
  }

}
