package com.elevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

/**
 * @author Rohan Raghuwanshi (rraghuw)
 *
 * 
 */
public class InputReader {

  private File inputsDirectory;

  public InputReader() {

  }

  public File getInputsDirectory() {
    return inputsDirectory;
  }

  public void setInputsDirectory(File inputsDirectory) {
    this.inputsDirectory = inputsDirectory;
  }

  public List<String> readLines() throws IOException {
    List<String> inputs = new ArrayList<String>();
    try {
      List<String> lines = FileUtils.readLines(inputsDirectory);
      for (String line : lines) {
        inputs.add(line);
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();

    } catch (IOException e) {
      e.printStackTrace();
    }
    return inputs;
  }
}
