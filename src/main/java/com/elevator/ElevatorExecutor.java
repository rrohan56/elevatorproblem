package com.elevator;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.elevator.exceptions.InvalidNumber;

/**
 * @author Rohan Raghuwanshi (rraghuw)
 *
 * 
 */
public class ElevatorExecutor {
  
  final static Queue<Integer> startFloor = new LinkedList<Integer>();
  final static Queue<Integer> endFloor = new LinkedList<Integer>();
  final static Queue<Integer> currentFloor = new LinkedList<Integer>();

  public static void main(String[] args) throws Exception {

    if (args.length < 2) {
      System.out.println("Please provide both the location of input directory and the mode to run the application");
      System.exit(1);
    }

    File inputDir = new File(args[0]);

    if (!inputDir.exists()) {
      System.out.println("Directory doesn't exist: " + inputDir.getAbsolutePath());
      System.exit(1);
    }
    String mode = args[1];
  
    if (!"ModeA".equalsIgnoreCase(mode) && !"ModeB".equalsIgnoreCase(mode)) {
      System.out.println("Invalid mode. Please enter a valid mode you want to run the application for");
      System.exit(1);
    }

    if ("ModeA".equalsIgnoreCase(mode)) {
      
      List<String> inputLines = readInput(inputDir);
      executeModeA(inputLines, startFloor, endFloor, currentFloor);
    } else {
     
      List<String> inputLines = readInput(inputDir);
      executeModeB(inputLines, startFloor, endFloor, currentFloor);

      }
    }

  private static void executeModeB(List<String> inputLines, Queue<Integer> startFloor2, Queue<Integer> endFloor2,
      Queue<Integer> currentFloor2) throws InvalidNumber {
    
    System.out.println("Executing Mode B...");
    for (int i = 0; i < inputLines.size(); i++) {
      String s = inputLines.get(i);
      String[] elements = s.split(":");
      Integer currentFloorLocation = Integer.parseInt(elements[0]);
      currentFloor2.add(currentFloorLocation);
      System.out.print(currentFloorLocation + " ");
      String[] startEndFloorPairs = elements[1].split(",");
      String[] startFloorString = null;
      for (int y = 0; y < startEndFloorPairs.length; y++) {

        startFloorString = startEndFloorPairs[y].split("-");
        startFloor2.add(Integer.parseInt(startFloorString[0]));
        endFloor2.add(Integer.parseInt(startFloorString[1]));
      }
      
      ModeB instance = new ModeB();
      instance.setCurrentLocation(currentFloor2);
      instance.setStartFloor(startFloor2);
      instance.setEndFloor(endFloor2);
      instance.getElevatorOutput();
      
      System.out.print("(" + instance.getFloorCount() + ")");
      System.out.println();
      instance.setFloorCount(0);
    } 
    
  }

  private static void executeModeA(List<String> inputLines, final Queue<Integer> startFloor,
      final Queue<Integer> endFloor, final Queue<Integer> currentFloor) throws InvalidNumber {
    
    System.out.println("Executing Mode A...");
    for (int i = 0; i < inputLines.size(); i++) {
      String s = inputLines.get(i);
      String[] elements = s.split(":");
      Integer currentFloorLocation = Integer.parseInt(elements[0]);
      currentFloor.add(currentFloorLocation);
      System.out.print(currentFloorLocation + " ");
      String[] startEndFloorPairs = elements[1].split(",");
      String[] startFloorString = null;
      for (int y = 0; y < startEndFloorPairs.length; y++) {

        startFloorString = startEndFloorPairs[y].split("-");
        for (int j = 0; j < startFloorString.length; j++) {
          System.out.print(startFloorString[j] + " ");
        }
        startFloor.add(Integer.parseInt(startFloorString[0]));
        endFloor.add(Integer.parseInt(startFloorString[1]));
      }

      ModeA instance = new ModeA();
      instance.setCurrentLocation(currentFloor);
      instance.setStartFloor(startFloor);
      instance.setEndFloor(endFloor);
      instance.getElevatorOutput();

      System.out.print("(" + instance.getFloorCount() + ")");
      System.out.println();
    }
  }

  private static List<String> readInput(File inputDir) throws IOException {
    InputReader reader = new InputReader();
    reader.setInputsDirectory(inputDir);
    List<String> inputLines = reader.readLines();
    return inputLines;
  }
}
