package com.elevator.core;

/**
 * @author Rohan Raghuwanshi (rraghuw)
 *
 * 
 */
public interface ElevatorSystemFactory {

  public void pickUp(Integer pickUpFloor);

  public void destination(Integer destinationFloor);

  public int getFloorCountModeA();

  public int getFloorCountModeB();
  
}
