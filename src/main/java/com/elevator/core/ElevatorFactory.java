package com.elevator.core;

/**
 * @author Rohan Raghuwanshi (rraghuw)
 *
 * 
 */
public interface ElevatorFactory {

  public void goUp();

  public void goDown();
  
  public void moveUp();
  
  public void moveDown();

}
