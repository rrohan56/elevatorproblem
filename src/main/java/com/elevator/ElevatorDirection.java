package com.elevator;
/**
 * @author Rohan Raghuwanshi (rraghuw)
 *
 * 
 */
public enum ElevatorDirection {

  UP, DOWN;
}
